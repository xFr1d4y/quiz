﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
public class Sensor : MonoBehaviour
{
    //This line will assignment a serial port to a variable, so you can access it later
    //Make sure "COM5" is the same port in the ArduinoIDE
    //9600 is the data rate in bits per second (baud), this should match the rate you send it at, default to 9600 i believe
    SerialPort ardIn = new SerialPort("COM6", 115200);

    //Variables that are used for an example.
    //The example being to make a cube jump when you press the button with a jump force given by the potentiometer
    public float jumpforce = 200.0f;
    public bool onGround = true;

    void Start()
    {
        //Open the serial port
        ardIn.Open();
        //Sets the number of milliseconds before timeout occurs when a read operation does not finish
        //If something goes wrong in your read operations, it will timeout after ___ amount of milliseconds
        ardIn.ReadTimeout = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //Check to see if the port is open
        if (ardIn.IsOpen)
        {
            // Try and run this code 
            try
            {
                //String to store our inputs from the arduino
                string input = ardIn.ReadLine();
                //printing out the input, just checking
                print(input);
                //we are spliting our input string when we see a ','
                //Example of input - 479,1 | 479 being out potentiometer, 1 button being pressed
                //After this line valueString=[479][1]
                string[] valueString = input.Split(',');
                //this.gameObject.transform.eulerAngles = new Vector3 (int.Parse(valueString[1]),int.Parse(valueString[2]),int.Parse(valueString[3]));
                this.transform.rotation = Quaternion.Euler(float.Parse(valueString[2]), float.Parse(valueString[1]), float.Parse(valueString[3]));

                transform.position += transform.up * 0.01f;

                jumpforce = int.Parse(valueString[4]);

                //Setting a max jumoforce of 500, if its more we are going to clamp it to 500
                if (jumpforce > 800.0f)
                {
                    jumpforce = 800.0f;
                }
                //If it is too low we clamp it to 200
                else if (jumpforce < 100.0f)
                {
                    jumpforce = 100.0f;
                }
                //We are checking if out second value in the string is confirming we are pressing the button
                if (valueString[5] == "1")
                {
                    jump();
                }



            }
            //If there are any erros run this code
            //With the system.exeption, will run the error that we get if there is one
            catch (System.Exception)
            {
            }
        }
    }


    //A simple jump funtion
    void jump()
    {
        if (!onGround || gameObject.GetComponent<Rigidbody>().velocity.y == 0)
        {
            onGround = true;
        }
        if (onGround)
        {
            gameObject.GetComponent<Rigidbody>().AddForce(transform.up * jumpforce);
            onGround = false;
        }
    }
}
